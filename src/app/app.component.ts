import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  public calculatorForm: FormGroup;
  public outcome = 0;
  public errorMessage = '';
  public argumentX = 0;
  public argumentY = 0;

  public constructor(private formBuilder: FormBuilder) {
    this.createFormGroup();
  }

  public divide(): void {
    this.errorMessage = '';
    if (this.argumentY > 0) {
      if (this.storeAndCheckIfArgumentsAreNumbers()) {
        this.outcome = this.argumentX / this.argumentY;
      }
    } else {
      this.errorMessage = 'The second argument has to be positive number';
      return;
    }
  }

  public multiply(): void {
    this.errorMessage = '';
    if (this.storeAndCheckIfArgumentsAreNumbers()) {
      this.outcome = this.argumentX * this.argumentY;
    }
  }

  public add(): void {
    this.errorMessage = '';
    if (this.storeAndCheckIfArgumentsAreNumbers()) {
      this.outcome = this.argumentX + this.argumentY;
    }
  }

  public subtract(): void {
    this.errorMessage = '';
    if (this.storeAndCheckIfArgumentsAreNumbers()) {
      this.outcome = this.argumentX - this.argumentY;
    }
  }

  public reset(): void {
    this.errorMessage = '';
    this.outcome = 0;
    this.calculatorForm.reset();
  }

  private createFormGroup(): void {
    this.calculatorForm = this.formBuilder.group({
      argumentX: undefined,
      argumentY: undefined
    });
  }

  private storeAndCheckIfArgumentsAreNumbers(): boolean {
    if (isNaN(this.calculatorForm.get('argumentX').value) || isNaN(this.calculatorForm.get('argumentY').value)) {
      this.errorMessage = 'The arguments have to be a number';
      return false;
    }
    this.argumentX = Number(this.calculatorForm.get('argumentX').value);
    this.argumentY = Number(this.calculatorForm.get('argumentY').value);
    return true;
  }
}
